from django.contrib import admin
from . import models
from modeltranslation.admin import TranslationAdmin

@admin.register(models.Product)
class PostAdmin(TranslationAdmin):
    list_display = ["id", "author", "title", "create_at", "category"]
    save_as = True #Сохранить как новый объект, чтобы нам не вписывать каждый раз новый пост, а просто его копировать
    save_on_top = True#Кнопка сохранить в админке будет и наверху, чтобы нам не листать вниз
    list_filter = ["category"]
    search_fields = ["category__name__startswith"]

@admin.register(models.Comment)
class CommentAdmin(TranslationAdmin):
    list_display = ['name', 'email', 'create_at', 'id']

@admin.register(models.Category)
class CategoryAdmin(TranslationAdmin):
    list_display = ("name", "slug")

