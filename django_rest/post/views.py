from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView
from .serializers import CommentCreateSerializer, ProductCreateSerializer, ProductSerializer
from .models import Product, Comment
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

class CreateAPIComment(ListCreateAPIView):
    def get_queryset(self):
        return Comment.objects.filter(product__id=self.kwargs.get("pk")).select_related('product')
    model = Comment
    serializer_class = CommentCreateSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )

    def perform_create(self, request):
        serializer = CommentCreateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class ProductViewSet(viewsets.ModelViewSet):
    # serializer_class = ProjectSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )
    queryset = Product.objects.all()

    def get_serializer_class(self):
        if self.action in ['create']:
            return ProductCreateSerializer
        return ProductSerializer

    @action(detail=True, methods=['post'])
    def file_create(self, request, pk=None):
        project = self.get_object()
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            project.file_project = serializer.validated_data['text']
            project.save()
            return Response({'status': 'text успешно изменен'})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['GET'], detail=False)
    def projects(self, request):
        projects = Product.objects.all()
        return Response({'product': [product.name for product in projects]})

