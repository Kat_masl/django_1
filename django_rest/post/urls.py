from django.urls import path
from .views import  ProductViewSet, CreateAPIComment
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'create', ProductViewSet)
router.register(r'create-product', ProductViewSet)


urlpatterns = [
    path('product/comment/<int:pk>/', CreateAPIComment.as_view(), name="post_comment"),
]
