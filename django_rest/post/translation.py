from modeltranslation.translator import register, TranslationOptions
from .models import Category, Product, Comment

@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(Product)
class PostTranslationOptions(TranslationOptions):
    fields = ('title', 'text')

@register(Comment)
class CommentTranslationOptions(TranslationOptions):
    fields = ('name', 'message')
