from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone

class Category(models.Model):
    name = models.CharField('Название', max_length=100)
    slug = models.SlugField('УФ URL', max_length=100)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

class Product(models.Model):
    author = models.ForeignKey( User, related_name="posts", verbose_name='Автор', on_delete=models.CASCADE)##Несколько авторов к одной категории
    title = models.CharField('Название', max_length=200)
    text = models.TextField('Текст')
    image = models.ImageField('Картинка', upload_to='articles/', null=True, blank=True)
    create_at = models.DateTimeField('Дата статьи', auto_now_add=True)
    category = models.ForeignKey(
        Category,
        related_name="product",
        verbose_name='Категории',
        on_delete=models.SET_NULL,##Удаляем категорию, а продукт остается
        null=True
    )
    slug = models.SlugField(max_length=200, default='', unique=True) #Уникальное поле


    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("post_single", kwargs={"slug": self.category.slug, "post_slug": self.slug})

    def get_comments(self):
        return self.comment.all()


class Comment(models.Model):
    name = models.CharField('Имя', max_length=50)
    email = models.CharField('Эмэил', max_length=100)
    message = models.TextField('Сообщение', max_length=500)
    create_at = models.DateTimeField('Дата коммента',default=timezone.now)
    post = models.ForeignKey(Product, related_name="comment", verbose_name='Посты', on_delete=models.CASCADE, null=True)##Удаляе кпост удаляются и комментарии

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

