from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Product, Comment


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']

class ProductCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'category', 'title', 'author', 'text', 'slug']

class ProductSerializer(serializers.ModelSerializer):
    post=serializers.SlugRelatedField(slug_field="title", read_only=True )
    class Meta:
        model = Product
        fields = ['id', 'category', 'title', 'author', 'text', 'slug']


class CommentCreateSerializer(serializers.ModelSerializer):
    # post=serializers.SlugRelatedField(slug_field="title", read_only=True )
    message = serializers.CharField()
    class Meta:
        model = Comment
        fields = ['name', 'email', 'message', 'post']

    def validate_message(self, value):
        if len(value) <10:
            raise serializers.ValidationError(f'Hазвание должно быть минимум 10 символов, вы ввели {len(value)}')
        return value
    
